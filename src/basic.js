$(document).ready(function() {
    function e(e) {
        var t = Math.ceil($(window).height() - .05 * $(window).height());
        return e ? t + "" + e : t
    }

    function t() {
        $(".overlay-email-contact").removeClass("check-input success"), $("#contact-form-submit").removeProp("disabled"), $(".overlay-bottom-bar").html(""), $(".overlay-email-contact .wrapper .success-message span").text("Thank you for your mail")
    }
    if (Modernizr.touch && $("html").addClass("touch"), Modernizr.inlinesvg || ($("html").addClass("no-svg"), $("svg").replaceWith(function() {
        return '<img src="' + $(this).attr("src") + '">'
    })), Modernizr.svg || ($("html").addClass("no-svg"), $('img[src$="svg"]').attr("src", function() {
        return $(this).attr("src").replace(".svg", ".png")
    })), navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
        var a = document.querySelector('meta[name="viewport"]');
        a && (a.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0", document.body.addEventListener("gesturestart", function() {
            a.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6"
        }, !1))
    }
    $("img, svg, .picture, .picture *, object").bind("contextmenu", function() {
        return !1
    }), $(".scroll-indicator").bind("click ", function() {
        $("html, body").animate({
            scrollTop: $(window).height()
        }, 800)
    });
    var r = 0;
    $(window).scroll(function() {
        var e = $(this).scrollTop();
        return 0 >= e ? ($("header").removeClass("asleep sticky"), void 0) : (e >= r && e > 180 ? $("header").addClass("asleep sticky") : $("header").removeClass("asleep").addClass("sticky"), r = e, void 0)
    }), $(window).bind("load resize", function() {
        var t = e("px"),
            a = $("#teaser .middle-content").outerHeight() + $("#teaser .bottom-content").outerHeight() + $("#teaser .wrapper").innerHeight() - $("#teaser .wrapper").height();
        parseInt(t) > a && $(window).width() > 767 ? $("#teaser").css("height", e("px")).removeClass("flexible") : $("#teaser").css("height", "auto").addClass("flexible")
    }), $(".overlay-email-contact-caller").bind("click", function() {
        $(".overlay-email-contact").addClass("show"), $("body").addClass("noscroll"), $(".overlay-email-contact .call-to-close").bind("click", function() {
            $(".overlay-email-contact").removeClass("show"), $("body").removeClass("noscroll"), t(), $(this).unbind()
        })
    }), $(".overlay-email-contact .fake-input").keyup(function() {
        var e = $(this).text().replace(/^\s\s*/, "").replace(/\s\s*$/, "");
        e.length < 1 ? $(this).addClass("empty") : $(this).removeClass("empty input-error")
    }).addClass("empty"), $("#contact-form-submit").bind("click", function(e) {
        e.preventDefault(), $(".overlay-email-contact").addClass("check-input"), $(this).prop("disabled", !0);
        var t = {
            name: $("#form-input-name").text(),
            email: $("#form-input-email").text(),
            message: $("#form-input-message").text(),
            subject: $("#form-input-subject").text()
        };
        $.ajax({
            type: "POST",
            url: "contact.php",
            dataType: "text",
            data: t,
            success: function(e) {
                switch (e) {
                    case "subject-error":
                        $("#form-input-subject").addClass("input-error"), $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled");
                        break;
                    case "email-error":
                        $("#form-input-email").addClass("input-error"), $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled");
                        break;
                    case "name-error":
                        $("#form-input-name").addClass("input-error"), $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled");
                        break;
                    case "try-again":
                        $(".overlay-bottom-bar").html("<span>Something went wrong. Please try again.</span>"), setTimeout(function() {
                            $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled")
                        }, 1e3);
                        break;
                    case "server-error":
                        $(".overlay-bottom-bar").html("<span>There is currently problem with the server.<br>Please try again later</span>"), setTimeout(function() {
                            $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled")
                        }, 1e3);
                        break;
                    case "success":
                        $(".overlay-email-contact .wrapper .success-message span").append(", <br>" + t.name.trim() + "."), setTimeout(function() {
                            $(".overlay-email-contact").addClass("success")
                        }, 1e3)
                }
            },
            error: function(e, t) {
                ("timeout" == t || "error" == t || "error" == t) && (t = "Something just went wrong.<br>Please try again later."), $(".overlay-bottom-bar").html("<span>" + t + "</span>"), setTimeout(function() {
                    $(".overlay-email-contact").removeClass("check-input"), $("#contact-form-submit").removeProp("disabled")
                }, 1e3)
            }
        })
    })
});